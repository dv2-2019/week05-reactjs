console.log("Hello, World")

// const x = "Hello ";
// x = "World";
// console.log(x)

// let x =10;
// var x= 5;
// console.log(x)


////////////////////////array ////////////////////////////
// var a = [5, "Hello", 5.7, false, function sum() {
//     return 5 + 10;
//     }
// ];
// const func = a[4];
// console.log(func());

//key associate array
// var info = {
//     name: "Meen",
//     age: 21,
// }
// const name = info.name;

//////////////////Obj destructuring /////////////////////
// var { name } = info;
// console.log(info)

// var text = "Hello";
// var [a, b, c, d, e] = text;
// console.log(a)

function sayHello(name, age = 10 ){
    alert("Hello" + name + ", Age : 21 "+ age );
}

function sum(){
    return 15;
}

///////////////function return function////////////////////
function getA(){
    return function getB(){
        return "Get B"
    }
}
var result = getA()
console.log(result())