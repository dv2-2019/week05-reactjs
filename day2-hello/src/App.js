import React from 'react';
import logo from './logo.svg';
import './App.css';

// function App() {
//   return (
//     <h1> Hello World</h1>
//   );
// }

//  const App = function () {
//   return (
//     <h1> Hello World</h1>
//   );
// }
const App = () => {
  return (
    <div>
      <Header />
      <Body />
      <Footer />
    </div>
  )
}

const Header = () => {
  return (
    <h1>Hello World!!</h1>
  )
}

const Body = () => {
  return (
    <p>Body</p>
  )
}
const Footer = () => {
  return (
    <h1> The Footer </h1>
  )

}
export default App;
